This project is for Institute for Information Industry(III) Project: 

The Pilot Study of Security Capabilities Analysis Technologies for User Equipment,

And the system was seperated into four parts:

1. ue-platform
2. [ue-platform-core](https://bitbucket.org/BlueLotus/ue-platform-core)
3. [ue-platform-web](https://bitbucket.org/BlueLotus/ue-platform-web)
4. [ue-platform-daemon](https://bitbucket.org/BlueLotus/ue-platform-daemon)

For setting up the system, please read the document at the following document:

http://ppt.cc/SM~b

If you want to see the demo video, click the following url:

http://ppt.cc/Ko2L